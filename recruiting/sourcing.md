# Sourcing

[Home](../index.md)

## Outbound

Outbound sourcing is analogous to sales - you go out and find the candidates.

In our current [hiring market for in India](./market.md), outbound is quick and easy to setup, but incredibly hard to scale.

Build this team like you'd build a sales team, with similar incentive structures and capabilities.



## Inbound

Inbound sourcing is analogous to marketing - you get candidates to come find you.

In our current [hiring market for in India](./market.md), inbound is slow and difficult to setup, but scales well and provides compounding returns.

Build this team like you'd build a marketing team, with similar incentive structures and capabilities.



## Candidate Pitch

Given the exteme gap between demand/supply, competent candidates aren't significantly different from customers to reason about. They are picky, have multiple options to choose from, are brand and price conscious and are incredibly hard to convert.

I recommend applying the usual product approaches like "segment your customer," "identify the customer's needs," "differentiate yourself from the competition" etc. etc. to define the 'product offering' to the candidate and create a candidate pitch deck. This pitch deck is used as input by both inbound and outbound when attempting to source and pitch candidates from that segment.

Every key candidate segment should have personas, and a targeted pitch for each persona.

The candidate pitch should address the following aspects that contribute to an employer brand:

1. Growth story of the business
2. Authenticity of the founders
3. Why are you a great place to work for that specific skillset/area (eg. design or engineering)
   1. Key differentiators from the competiton in terms of employee experience
      1. Compensation
      2. Leadership
         1. Mentoring
         2. Company Philosophy
      3. Execution
         1. Processes
         2. Tech stacks
         3. Methodologies
         4. Tools
         5. Metrics
      4. Results
         1. This is the same as 1 - Growth story of the business

